<?php get_header(); ?>
<div class="wrapper">
    <div class="container blog">
        <div class="row">
            <div class="col-md-offset-1 col-md-1">
                <h1><img src="<?php bloginfo('stylesheet_directory'); ?>/img/cash-thing48x48.png" alt="..." class="img-circle"></h1>
            </div>
            <div class="col-md-8">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <h1><?php the_title(); ?></h1>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p>&nbsp;<em><?php the_time('l, F jS, Y') ?></em></p>
                        <?php the_content(); ?>
                    </div>
                </div>
                <?php endwhile; else: ?>
                    <p><?php _e('Sorry, this page does not exist.'); ?></p>
                <?php endif; ?>
              </div>
              <div class="col-md-4">
                <?php get_sidebar(); ?>
              </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
