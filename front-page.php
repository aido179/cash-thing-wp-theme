
<?php get_header(); ?>
<!-- Full Width Image Header with Logo -->
    <!-- Image backgrounds are set within the full-width-pics.css file. -->
    <header class="image-bg-fluid-height  landing-header-container">
		<div class="container">
			<div class="row">
				<p><span class="landing-header"><b>Really Simple</b> Point of Sale</span></p>
			</div>
			<div class="row">
				<p class="landing-copy">For market vendors and small businesses.</p>
			</div>
			<div class="row">
				<div class="col-md-offset-4 col-md-4 landing-action" onclick="location.href='http://app.cash-thing.com/signup.php';">
					<span class="landing-sub-header">Sign up</p>
				</div>
			</div>
		</div>
    </header>
   <section>
        <div class="container">
            <div class="row">
				<div class="col-md-6 col-lg-8">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/screenshot1.png" class="img-responsive">
				</div>
				<div class="col-md-6 col-lg-4">
					<p><span class="landing-sub-header"><B>Made for market vendors.</b></p>
					<p><span class="landing-sub-header">Track transactions.</span></p>
					<p><span class="landing-sub-header">Generate daily reports.</span></p>
				</div>
			</div>
		</div>
	</section>
    <!-- Content Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
					<div class="row text-center">
						<div class="col-md-4">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/img/work-offline-128.png">
							<p class="landing-copy">Works Offline</p>
							<p>Don't let an internet connection tether your sales down. Sell from anywhere.</p>
						</div>
						<div class="col-md-4">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/img/Tablet-128.png">
							<p class="landing-copy">Cross Platform</p>
							<p>Whatever device you own or feel comfortable with - all you need is a web browser.</p>
						</div>
						<div class="col-md-4">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/img/Cash-128.png">
							<p class="landing-copy">Cost Effective</p>
							<p>At just <a href="prices.html">&#8364;10 per month</a>, Cash-thing is one third the cost of the leading competitor.</p>
						</div>
					</div>
				</div>
            </div>
        </div>
    </section>

    <!-- Content Section -->

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="landing-sub-header">Loaded with features:</p>
					<ul class="landing-list">
						<li>Track sales offline.</li>
						<li>Track cash and card sales separately.</li>
						<li>Add discounts as percentage or fixed amount.</li>
						<li>Manage stock online.</li>
						<li>Categorize stock.</li>
						<li>Generate re-stock shopping list.</li>
						<li>Track busy periods.</li>
						<li>Calculate revenue versus profit.</li>
					</ul>
				</div>
				<div class="col-md-6">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/img/screenshot2.png" class="img-responsive">
				</div>
            </div>
        </div>
        <!-- /.container -->
	<section>
		<div class="container">
			<div class="row">
                <div class="col-lg-12">
					<div class="row text-center">
						<div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 landing-action" onclick="location.href='http://app.cash-thing.com/signup.php';">
							<span class="landing-header">Try Cash-thing Now For Free</p>
						</div>
					</div>
					<div class="row text-center">
						<div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8">
							<p class="landing-copy">30 days free. &#8364;10 per month after.</p>
						</div>
					</div>
				</div>
            </div>
		</div>
	</section>

<div class="modal fade" id="signin-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Sign in</h4>
      </div>
      <div class="modal-body">
        <form id="loginForm" class="form-horizontal" role="form">
		  <div class="form-group">
			<label for="inputEmail3" class="col-sm-4 control-label text-right">Username</label>
			<div class="col-sm-8">
			  <input type="text" class="form-control" id="username" placeholder="Username">
			</div>
		  </div>
		  <div class="form-group">
			<label for="inputPassword3" class="col-sm-4 control-label">Password</label>
			<div class="col-sm-8">
			  <input type="password" class="form-control" id="pass" placeholder="Password">
			</div>
		  </div>
		  <div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
			  <div class="checkbox">
				<label>
				  <input id="rememberme" type="checkbox" > Remember me
				</label>
			  </div>
			</div>
		  </div>
		  <div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
			  <button type="submit" class="btn btn-primary">Sign in</button>
			</div>
		  </div>
		</form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php get_footer(); ?>
