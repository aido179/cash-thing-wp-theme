<!DOCTYPE html>
<html lang="en" manifest="offline.manifest.php">
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="application-name" content="Cash-thing"/>
        <meta name="description" content="Really Simple Point Of Sale"/>
        <meta name="application-url" content="http://www.cash-thing.com"/>
        <link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/img/cash-thing32x32.png" sizes="32x32"/>
        <link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/img/cash-thing48x48.png" sizes="48x48"/>
        <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
        <!--<title>Cash-thing.com</title>-->
        <title><?php wp_title('|',1,'right'); ?> <?php bloginfo('name'); ?></title>
        <?php wp_head(); ?>
    </head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <?php
          // Fix menu overlap bug..
          if ( is_admin_bar_showing() ) echo '<div style="min-height: 28px;"></div>';
        ?>
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo site_url(); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/cash-thinglogo.png"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a id="signup" href="http://app.cash-thing.com/signup.php">Sign up</a></li>
                    <li><a href="http://app.cash-thing.com">Sign in</a></li>
                    <?php wp_list_pages(array('title_li' => '', 'exclude'=> get_option( 'page_on_front' ))); ?>
				</ul>
			</div>
		</div>
	</nav>
