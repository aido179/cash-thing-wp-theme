
//Google analytics
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-44045677-1', 'auto');
ga('send', 'pageview');

//Home page js for login

function login(){
	var u = document.getElementById("username").value;
	var pass = document.getElementById("pass").value;
	var remember = $("#rememberme").is(":checked");
	$.ajax({
		type: "POST",
		url: "php/users.php",
		data: { 'type':'login','username' : u, 'pass': pass, 'rememberme':remember},
		//contentType: "application/json; charset=utf-8",
		dataType: "html",
		success: function(data){location.reload();},
		failure: function(errMsg) {
			alert("Sorry, something went wrong. We could not log you in.");
		}
	});
}
$('#loginForm').on('submit', function () {
    login();
    return false;
});
$("#bgimage").bind("load", function () { $(this).fadeIn(); });
