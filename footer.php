        <div class="footer hidden-xs hidden-sm" >
        	<div class="container" >
        		<div class="row">
        			<div class="col-md-4 text-center">
        				<p class="text-muted landing-copy"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/twitter-bird-48.png"> <a href="http://www.twitter.com/cashthingapp">@cashthingapp</a></p>
        			</div>
        			<div class="col-md-4 text-center">
        				<p class="text-muted landing-copy"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/mail-48.png"> aidan@cash-thing.com</p>
        			</div>
        			<div class="col-md-4 text-center">
        				<p class="text-muted landing-copy"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/Newspaper-48.png"> <a href="<?php echo _get_page_link( get_option( 'page_for_posts' ) ); ?>">Blog</a></p>
        			</div>
        		</div>
        	</div>
        </div>

        <?php wp_footer(); ?>
    </body>
</html>
