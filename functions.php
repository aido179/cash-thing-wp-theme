<?php

function cash_thing_scripts()
{

    wp_register_script( 'cashthingjquery', get_template_directory_uri() . '/js/jquery.min.js', array() );
    wp_enqueue_script( 'cashthingjquery' );

	wp_register_script( 'cashthingbootstrap', get_template_directory_uri() . '/js/bootstrap.js', array( 'cashthingjquery' ) );
	wp_enqueue_script( 'cashthingbootstrap' );

    wp_register_script( 'cashthingjs', get_template_directory_uri() . '/js/cashthing.js', array() );
    wp_enqueue_script( 'cashthingjs' );
}
add_action( 'wp_enqueue_scripts', 'cash_thing_scripts' );

?>


<?php

function wpbootstrap_scripts_with_jquery()
{
	// Register the script like this for a theme:
	wp_register_script( 'custom-script', get_template_directory_uri() . '/bootstrap/js/bootstrap.js', array( 'jquery' ) );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
?>
